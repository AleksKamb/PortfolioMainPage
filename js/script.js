$(document).ready(function () {
            
    $('#fullpage').fullpage({
        
        sectionsColor: ['white', 'whitesmoke','white'],
        anchors: ['intro', 'about-me', 'projects'],
        licenseKey:"OPEN-SOURCE-GPLV3-LICENSE",
     
    }); 

        $('button').click(function () {
            var circle = $('#circle');


            circle.addClass("expand").removeClass('.circle');
            circle.removeClass('flex');
            $(".hide").removeClass('hide');
            $(".vertical").animate({ "bottom": "5%", "opacity": "1" }, 1500);

            if (window.matchMedia('(max-width: 767px)').matches) {
                setInterval(function () { $(".expand-fill").animate({ width: '49%', }, "slow"); }, 500);
            }
            else {
                setInterval(function () { $(".expand-fill").animate({ width: '50%', }, "slow"); }, 500);
            }
            setInterval(function () { $('#heading').fadeIn(1200); });
            $('#btn').fadeOut(2, 'swing');
            $("#enter").fadeOut(2, 'swing');

        });

        $("#btn").hover(function () {
            $("#enter").addClass("line-extend");

        }, function () {
            $("#enter").removeClass("line-extend");
        });

        //Checks if the section classes have changed in order to animate the second arrow 
        setInterval(function () {
            if ($(".about").hasClass("active") == true && $(".hide").hasClass("hide") == false) {
                $(".about").trigger("showArrow");

            }
            else if ($(".about").hasClass("active") == false && $(".hide").hasClass('hide') == false) {
                function hide() {
                    $(".vertical-fixed").fadeOut(350);
                    $(".vertical").fadeIn("slow");

                };
                hide();
            }
            $('.about').on('showArrow', function () {
                $(".vertical-fixed").fadeIn("slow");
                $(".vertical").fadeOut(350);
            });
        }, 100)
        //checks if projects page is active and triggers the animation
        setInterval(function () {
            if ($(".projects").hasClass("active")) {
                $(".projects").trigger("pAnimate");

            }

            $('.projects').one('pAnimate', function () {
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $('.expand-top').animate({ height: '25rem' }, "slow", 'linear', function () {
                    $('.project').fadeIn("slow");

                });
                } else {
                    $('.expand-top').animate({ height: '50%' }, "slow", 'linear', function () {
                    $('.project').fadeIn("slow");

                });
                }
               
                $('.other-projects').fadeIn("slow");
            });
        }, 100)
        // Tilt library for the project buttons
        $('.project').tilt({

            scale: 1.1,


        })
        //"breathing" dot animation
        setInterval(function () {
            $(".dot").animate({ opacity: "1", height: "4rem", width: "4rem" }, "slow")
                .animate({ opacity: "0", height: "1rem", width: "1rem" }, "slow");
        }, 2000)

        $(".project").hover(function () {
            $(".info-ball").animate({ borderRadius: "0%", height: "90%", width: "50%", }, "slow");
            $(".dot").css("display", "none");


        });
        $("#gh").hover(function () {
            $(".info-text").fadeIn(100);
            $(".info-text").text("Calculates the time when light is the softest.")


        }, function () {
            $(".info-text").fadeOut(100);
        });

        $("#hed").hover(function () {
            $(".info-text").fadeIn(100);
            $(".info-text").text("The Hedonist - a modern approach for the hospitality industry.")


        }, function () {
            $(".info-text").fadeOut(100);
        });

        $("#rso").hover(function () {
            $(".info-text").fadeIn(100);
            $(".info-text").text("RSO Hosting's company website - a team endavour.")


        }, function () {
            $(".info-text").fadeOut(100);;
        });

        $("#learned").hover(function () {
            $(".info-text").fadeIn(100);
            $(".info-text").text("The dutch company Learned.io's website - a two person project")


        }, function () {
            $(".info-text").fadeOut(100);
        });

    });






